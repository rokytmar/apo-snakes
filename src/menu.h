/*******************************************************************
  menu.h 	- menu and welcome screen


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef MENU_H
#define MENU_H

#include <stdlib.h>
#include <time.h>

#include "graphical_manager.h"
#include "peripheral_manager.h"
#include "control_manager.h"
#include "label_manager.h"
#include "colors.h"
#include "booleans.h"
#include "game.h"
#include "menu_settings.h"

#define TITLE "Snake!"

#define START_BUTTON 0
#define SETTINGS_BUTTON 1
#define EXIT_BUTTON 2

#ifndef GAME_TYPE
#define GAME_TYPE
typedef struct {
	int game_running;		//Game running flag
	int regime;				//Game regime selector
	int game_speed;			//Game speed selector
	uint16_t snake1_color;	//Color of first snake
	uint16_t snake2_color;	//Color of second snake
} game_t;
#endif

int show_menu(game_t* game);


#endif