/*******************************************************************
  peripheral_manager.h 	- Simple peripheral parser


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef PERIPHERAL_MANAGER_H
#define PERIPHERAL_MANAGER_H

#include <stdlib.h>
#include <stdio.h>

#include "booleans.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "mzapo_parlcd.h"
#include "colors.h"

typedef struct {
	int red_pressed;	// TRUE if red knob was pressed
	int green_pressed;	// TRUE if green knob was pressed
	int blue_pressed;	// TRUE if blue knob was pressed
	uint8_t red_val;	// State of red knob
	uint8_t green_val;	// State of green knob
	uint8_t blue_val;	// State of blue knob
} knobs_t;


knobs_t* get_knobs();

void set_knobs();

void init_peripherals();

void light_left_led(uint32_t color);

void light_right_led(uint32_t color);

void light_led_belt(uint32_t value);

void light_belt_bits(int num1, int num2);

void reset_peripherals();

#endif