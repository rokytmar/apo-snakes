/*******************************************************************
  map.hc	- Declarations and utilities of map, with positioning system and basic collision resolver


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "map.h"

/*
*	Returns TRUE if specified position is in one of snakes body
*/
int pos_in_snake(int x, int y, snake_t* snake1, snake_t* snake2, int exclude_head) {
	body_node* node = snake1->head;
	if(exclude_head) {
		node = node->next;
	}
	//Not checking tails, because of future move
	while (node != NULL) {
		if(x == node->x && y == node->y) {
			return TRUE;
		}
		node = node->next;
	}

	node = snake2->head;

	while (node != NULL) {
		if(x == node->x && y == node->y) {
			return TRUE;
		}
		node = node->next;
	}
	return FALSE;
}

/*
*	Generate new positions for food
*/
void generate_new_food(food_t* food) {
	food->x = rand() % MAP_WIDTH;
	food->y = rand() % MAP_HEIGHT;
}

/*
*	Detect collision and resolve the collision
*/
void detect_collision(snake_t* snake, snake_t* opponent, food_t* food) {
	if(snake->head->x == food->x && snake->head->y == food->y) {
		enlarge_snake(snake);
		generate_new_food(food);
		while(pos_in_snake(food->x, food->y, snake, opponent, FALSE)) {
			generate_new_food(food);
		}
		snake->state = FEEDING;
	} else if (detect_crash(snake, opponent)) {
		snake->state = DEAD;
	} else {
		snake->state = HEALTY;	
	}
}

/*
*	Detect snake crashed
*/
int detect_crash(snake_t* snake, snake_t* opponent) {
	if(pos_in_snake(snake->head->x, snake->head->y, snake, opponent, TRUE)) {
		return TRUE;
	}

	int outx = snake->head->x < 0 || snake->head->x > MAP_WIDTH ? TRUE : FALSE;
	int outy = snake->head->y < 0 || snake->head->y > MAP_HEIGHT ? TRUE : FALSE;
	return outx || outy;
}
