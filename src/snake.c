/*******************************************************************
  snake.h 	- Declarations and utilities of snake entity


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "snake.h"
#include "map.h"

/*
* 	Set heading by turning left
*/
void turn_left(snake_t* snake) {
	switch(snake->heading) {
		case UP:
			snake->heading = LEFT;
			break;
		case RIGHT:
			snake->heading = UP;
			break;
		case DOWN:
			snake->heading = RIGHT;
			break;
		case LEFT:
			snake->heading = DOWN;
			break;
	}
}


/*
* 	Set heading by turning right
*/
void turn_right(snake_t* snake) {
	switch(snake->heading) {
		case UP:
			snake->heading = RIGHT;
			break;
		case RIGHT:
			snake->heading = DOWN;
			break;
		case DOWN:
			snake->heading = LEFT;
			break;
		case LEFT:
			snake->heading = UP;
			break;
	}
}

/*
*	Move all body positions to previous body node location
*/
void move(snake_t* snake) {
	body_node* node = snake->head;
	int prev_x = node->x;
	int prev_y = node->y;

	switch(snake->heading) {
		case UP:
			node->y--;
			break;
		case DOWN:
			node->y++;
			break;
		case RIGHT:
			node->x++;
			break;
		case LEFT:
			node->x--;
			break;
	}

	int next_x;
	int next_y;

	while(node->next != NULL) {
		node = node->next;
		next_x = node->x;
		next_y = node->y;
		node->x = prev_x;
		node->y = prev_y;
		prev_x = next_x;
		prev_y = next_y;
	}
}

/*
*	Add new tile and add length
*/
void enlarge_snake(snake_t* snake) {
	snake->length++;
	body_node* old_tail = snake->tail;
	body_node* new_tail = (body_node*)malloc(sizeof(body_node));
	new_tail->x = old_tail->x;
	new_tail->y = old_tail->y;
	new_tail->next = NULL;
	old_tail->next = new_tail;
	snake->tail = new_tail;
}

/*
*	Generate new snake
*/
snake_t* generate_snake(uint16_t color, int snake_id) {
	snake_t* snake = (snake_t*)malloc(sizeof(snake_t));
	snake->heading = RIGHT;
	snake->length = 2;
	body_node* head = (body_node*)malloc(sizeof(body_node));
	head->x = MAP_WIDTH / 2;
	head->y = MAP_HEIGHT / 2;
	if(snake_id == 2) {
		head->y++;
	}
	body_node* tail = (body_node*)malloc(sizeof(body_node));
	tail->x = head->x - 1;
	tail->y = head->y;
	head->next = tail;
	tail->next = NULL;

	snake->head = head;
	snake->tail = tail;

	snake->color = color;
	snake->state = HEALTY;
	return snake;
}
