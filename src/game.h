/*******************************************************************
  game.h 	- Main game loop and stage handling


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef GAME_H
#define GAME_H

#include <stdlib.h>
#include <stdint.h>

#include "booleans.h"
#include "colors.h"
#include "menu.h"
#include "snake.h"
#include "map.h"
#include "ai_helper.h"

#include "game_graphics.h"
#include "peripheral_manager.h"
#include "control_manager.h"

#define STATUS_BAR_WIDTH 80

#define P_V_AI 0
#define P_V_P 1
#define AI_V_AI 2

// Game speeds macros are defined by number of miliseconds to wait between ticks
#define SLOW 8		
#define MEDIUM 4
#define FAST 1

#define DEFAULT_SPEED MEDIUM	
#define DEF_COLOR_1 BLUE
#define DEF_COLOR_2 RED

#ifndef GAME_TYPE
#define GAME_TYPE
typedef struct {
	int game_running;		//Game running flag
	int regime;				//Game regime selector
	int game_speed;			//Game speed selector
	uint16_t snake1_color;	//Color of first snake
	uint16_t snake2_color;	//Color of second snake
} game_t;
#endif

int run_game_loop(game_t* game, snake_t* snake1, snake_t* snake2, food_t* food);

void start_game(); 


#endif