/*******************************************************************
  ai_helper.h 	- Declarations and utilities of artificial inteligence


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef AI_HELPER_H
#define AI_HELPER_H

#include <stdlib.h>
#include <stdint.h>

#include "map.h"
#include "snake.h"

void decide_snake_action(snake_t* snake, snake_t* opponent, food_t* food);

#endif