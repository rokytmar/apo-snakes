/*******************************************************************
  game.h 	- Main game loop and stage handling


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/
#include "game.h"

/*
*   Free allocated snake linked list members
*/  
void destroy_snake(snake_t* snake) {
    body_node* node = snake->head;
    body_node* next_node;
    while(node != NULL) {
        next_node = node->next;
        free(node);
        node = next_node;
    }
    free(snake);
}

/*
*   Change snake positions by controls
*/
void get_snake_actions_from_controls(snake_t* snake1, snake_t* snake2) {
    control_status_t* controls = get_controls();

    //If in one player regime, bing both player controls to player one
    if(controls->p1right || (snake2 == NULL && controls->p2right)) {
            snake1->heading++;
            snake1->heading %= 4;
    } else if (controls->p1left || (snake2 == NULL && controls->p2left)) {
        snake1->heading--;
        if(snake1->heading == -1) {
            snake1->heading = 3;
        }
    }

    if(snake2 != NULL && controls->p2right) {
        snake2->heading++;
        snake2->heading %= 4;
    } else if (snake2 != NULL && controls->p2left) {
        snake2->heading--;
        if(snake2->heading == -1) {
            snake2->heading = 3;
        }
    }
}

/*
* Basic game loop updating game state and calling rendering
* returns id of winner
*/
int run_game_loop(game_t* game, snake_t* snake1, snake_t* snake2, food_t* food) {
    int winner = 0;
    while(game->game_running) {
        // Decide how to decide about snakes next move based on regime
        if(game->regime == P_V_AI) {
            get_snake_actions_from_controls(snake1, NULL);
            decide_snake_action(snake2, snake1, food);
        } else if (game->regime == AI_V_AI) {
            decide_snake_action(snake2, snake1, food);
            decide_snake_action(snake1, snake2, food);
        } else if(game->regime == P_V_P) {
            get_snake_actions_from_controls(snake1, snake2);
        }
        // Move snakes
        move(snake1);
        move(snake2);
        // Detect collisions to eat food or end game
        detect_collision(snake1, snake2, food);
        detect_collision(snake2, snake1, food);

        // Draw game state to display
        draw_game(snake1, snake2, food);
        // Light leds according to snake state
        light_left_led(snake1->state);
        light_right_led(snake2->state);
        // Light led belt according to snake lengths
        light_belt_bits(snake1->length / 2, snake2->length / 2);

        // Wait for time length specified in game speed
        struct timespec waiter = {.tv_sec = 0, .tv_nsec = game->game_speed * 100000000};
        clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);
        // Set runnig flag to false to determine later
        game->game_running = FALSE;
        if(snake1->length == MAX_LENGTH) {
            winner = 1;
        } else if (snake2->length == MAX_LENGTH) {
            winner = 2;
        } else if(snake1->state == DEAD) {
            winner = 2;
        } else if(snake2->state == DEAD) {
            winner = 1;
        } else {
            // Continue game of neither player won or crashed
            game->game_running = TRUE;
        }
    }
    struct timespec waiter = {.tv_sec = 1, .tv_nsec = 0};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);
    return winner;
}

/*
*   Select regime to play in
*/
void select_regime(game_t* game) {
    int button_selector = P_V_AI;
    int option_selected = FALSE;
    while(!option_selected) {
        reset_frame();
        // Redraw labels to show selectin by underlining
        add_label("P v AI", GREEN, 4, button_selector == P_V_AI ? TRUE : FALSE, 0, -DISPLAY_HEIGHT / 2);
        add_label("P v P", BLUE, 4, button_selector == P_V_P ? TRUE : FALSE, 0, 0);
        add_label("AI v AI", RED, 4, button_selector == AI_V_AI ? TRUE : FALSE, 0, DISPLAY_HEIGHT / 2);
        refresh_display();

        control_status_t* controls = get_controls();

        // Handle controls
        if(controls->select) {
            option_selected = TRUE;
        }
        if(controls->p1right || controls->p2right) {
            button_selector++;
            button_selector %= 3;
        }
        if(controls->p1left || controls->p2left) {
            button_selector--;
            button_selector = button_selector < 0 ? 2 : button_selector;
            button_selector %= 3;
        }
    }
    game->regime = button_selector;
}

/*
*   Start the game
*/
void start_game() {
    game_t* game = (game_t*)malloc(sizeof(game_t));
    game->regime = MEDIUM;
    game->game_speed = DEFAULT_SPEED;
    game->snake1_color = DEF_COLOR_1;
    game->snake2_color = DEF_COLOR_2;
    
    while(TRUE) {
        printf("menu started\n");
        if(!show_menu(game)) {
            printf("Exit...");
            break;
        }

        // Show menu to select regime
        select_regime(game);

        printf("game started\n");
        game->game_running = TRUE;
        // Generate food and snakes
        food_t* food = (food_t*)malloc(sizeof(food_t));
        generate_new_food(food);
        snake_t* snake1 = generate_snake(game->snake1_color, 1);
        snake_t* snake2 = generate_snake(game->snake2_color, 2);
        
        // Run game loop to get winner
        int winner = run_game_loop(game, snake1, snake2, food);
        // Show who won on screen and wait
        show_winner(winner, winner == 1 ? snake1->color : snake2->color);
        struct timespec waiter = {.tv_sec = 2, .tv_nsec = 0};
        clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);

        // Free food and snades
        free(food);
        destroy_snake(snake1);
        destroy_snake(snake2);
    }
    
    free(game);
}
