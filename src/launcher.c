/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  launcher.c - main file


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <time.h>

#include "graphical_manager.h"
#include "peripheral_manager.h"
#include "control_manager.h"
#include "game.h"

/*
* Main function initializing all managers, starting game, and then terminating managers
*/
int main(int argc, char *argv[])
{
  // Generate new seed for future random position generation
  srand(time(NULL));

  // Initialize managers to create all static instances
  init_graphics();
  init_peripherals();
  init_input();

  //Launch the game
  start_game();

  // Return to original state and terminate mamangers
  reset_input();
  terminate_graphics();
  reset_peripherals();
  return 0;
}
