/*******************************************************************
  menu_settings.c 	- Menu page for setting selection


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "menu_settings.h"

/*
*	Cycle colors for snake
*/
void change_snake_color(uint16_t* color) {
	switch(*color) {
		case BLUE:
			*color = RED;
			break;
		case RED:
			*color = GREEN;
			break;
		case GREEN:
			*color = BLUE;
			break;
	}
}

/*
*	Light led to snakes'c color, choosing 32bit equivalent of the color
*/
void show_color_on_led(int snake_id, uint16_t color) {
	switch(color) {
		case RED:
			if(snake_id == 1) {
				light_left_led(RED_32);
			} else {
				light_right_led(RED_32);
			}
			break;
		case GREEN:
			if(snake_id == 1) {
				light_left_led(GREEN_32);
			} else {
				light_right_led(GREEN_32);
			}
			break;
		case BLUE:
			if(snake_id == 1) {
				light_left_led(BLUE_32);
			} else {
				light_right_led(BLUE_32);
			}
			break;
	}
}

/*
*	Show settings menu
*/
void show_settings(game_t* game) {
	//Speed selector to static to remember last setting
	static int button_selector = SPEED_SELECTOR;
	int in_menu = TRUE;
	while(in_menu) {
		int option_selected = FALSE;
		while(!option_selected) {
			reset_frame();
			// Refresh labels to underline selected label
			add_label("Speed", PINK, 2, button_selector == SPEED_SELECTOR ? TRUE : FALSE, -DISPLAY_WIDTH / 2, -DISPLAY_HEIGHT / 2);
			add_label("Snake 1 color", WHITE, 2, button_selector == P1_COLOR_SELECTOR ? TRUE : FALSE, -DISPLAY_WIDTH / 2, -DISPLAY_HEIGHT / 4);
			add_label("Snake 2 color", WHITE, 2, button_selector == P2_COLOR_SELECTOR ? TRUE : FALSE, -DISPLAY_WIDTH / 2, 0);
			add_label("Back to menu", RED, 3, button_selector == BACK_BUTTON ? TRUE : FALSE, 0, DISPLAY_HEIGHT / 2);

			// Add accurate label to current game speed selection 
			char* speed_label;
			if(game->game_speed == SLOW) {
				speed_label = SLOW_LABEL;
			} else if (game->game_speed == MEDIUM) {
				speed_label = MEDIUM_LABEL;
			} else {
				speed_label = FAST_LABEL;
			}
			add_label(speed_label, PINK, 2, FALSE, DISPLAY_WIDTH / 2, -DISPLAY_HEIGHT / 2);

			// Fill rectangles representing snake color to current selection
			fill_rectangle_with_color(3 * DISPLAY_WIDTH / 4 - COLOR_TILE_SIZE / 2, 3 * DISPLAY_HEIGHT / 8 - COLOR_TILE_SIZE / 2, COLOR_TILE_SIZE, COLOR_TILE_SIZE, game->snake1_color);
			fill_rectangle_with_color(3 * DISPLAY_WIDTH / 4 - COLOR_TILE_SIZE / 2, DISPLAY_HEIGHT / 2 - COLOR_TILE_SIZE / 2, COLOR_TILE_SIZE, COLOR_TILE_SIZE, game->snake2_color);
			refresh_display();
			
			// Show snake color on led
			show_color_on_led(1, game->snake1_color);
			show_color_on_led(2, game->snake2_color);

			control_status_t* controls = get_controls();

			// Handle controls to change selection or select current one
			if(controls->select) {
				option_selected = TRUE;
			}
			if(controls->p1right || controls->p2right) {
				button_selector++;
				button_selector %= 4;
			}
			if(controls->p1left || controls->p2left) {
				button_selector--;
				button_selector = button_selector < 0 ? 3 : button_selector;
				button_selector %= 4;
			}
		}

		// Handle selcection
		switch(button_selector) {
			case SPEED_SELECTOR:
				//Change game speed
				switch(game->game_speed) {
					case SLOW:
						game->game_speed = MEDIUM;
						break;
					case MEDIUM:
						game->game_speed = FAST;
						break;
					case FAST:
						game->game_speed = SLOW;
						break;
				}
				break;
			case P1_COLOR_SELECTOR:
				//Change snake 1 color
				change_snake_color(&(game->snake1_color));
				break;
			case P2_COLOR_SELECTOR:
				//Change snake 2 color
				change_snake_color(&(game->snake2_color));
				break;
			case BACK_BUTTON:
				// Return to main menu
				in_menu = FALSE;
				break;
		}
	}
}
