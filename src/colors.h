/*******************************************************************
  colors.h 	- Headers defining basic colors in rgb565


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef COLORS_H
#define COLORS_H

#define RED_32 0xffb00000
#define GREEN_32 0x003ffb00
#define BLUE_32 0x000003ff

#define RED 0x8800
#define GREEN 0x04e0	
#define BLUE 0x000b
#define PINK 0xf00f
#define WHITE 0xffff
#define BLACK 0x0u

#endif