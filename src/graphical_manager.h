/*******************************************************************
  graphical_manager.h 	- Simple paralel lcd driver


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef GRAPHICAL_MANAGER_H
#define GRAPHICAL_MANAGER_H

#include <stdlib.h>
#include <stdio.h>

#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "mzapo_parlcd.h"

#include "colors.h"

#define DISPLAY_WIDTH 480
#define DISPLAY_HEIGHT 320

#define DISPLAY_WRITE 0x2c

uint16_t* get_frame();

void init_graphics();

void reset_frame();

void reset_display();

void fill_frame_with_color(uint16_t color);

void fill_rectangle_with_color(int posx, int posy, int width, int height, uint16_t color);

void put_tile_to_frame(uint16_t color, int x, int y, int tile_size);

void refresh_display();

void terminate_graphics();

#endif