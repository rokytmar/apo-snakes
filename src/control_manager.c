/*******************************************************************
  control_manager.c 	- Declarations and utilities of controls and control handling


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "control_manager.h"

static int flags;
// Backup struct to store last terminal settings
static struct termios termios_backup;
// Last registered positions of knobs, to determine change
static knobs_t* last_knobs;

static control_status_t* control_status;

/*
*	Gets control from knob state by change of state
*/
void bind_controls_from_knobs() {
	knobs_t* now = get_knobs();
	// Always update when knob was pressed
	if(now->red_pressed || now->green_pressed || now->blue_pressed) {
		control_status->select = TRUE;
	} else {
		// Match positions against last known state and set control flags accordingly
		if(now->red_val != last_knobs->red_val) {
			if(now->red_val > last_knobs->red_pressed) {
				control_status->p1right = TRUE;
			}
			if(now->red_val < last_knobs->red_pressed) {
				control_status->p1left = TRUE;
			}
		}
		if(now->blue_val != last_knobs->blue_val) {
			if(now->blue_val > last_knobs->blue_pressed) {
				control_status->p2right = TRUE;
			}
			if(now->blue_val < last_knobs->blue_pressed) {
				control_status->p2left = TRUE;
			}
		}
	}
}

/*
*	Returns true if knobs has changed since last check
*/
int knobs_changed() {
	knobs_t* now = get_knobs();
	// Return TRUE if any of knobs was pressed or position has changed
	if( !(now->red_pressed) &&
		!(now->blue_pressed) &&
		!(now->green_pressed)  &&
		now->red_val == last_knobs->red_val &&
		now->green_val == last_knobs->green_val &&
		now->blue_val == last_knobs->blue_val) {
		return FALSE;
	}
	return TRUE;
}

/*
*	Sets current position of knobs to last variable. Should be made after check and update.
*/
void copy_knobs() {
	knobs_t* now = get_knobs();
	last_knobs->red_pressed = now->red_pressed;
  	last_knobs->green_pressed = now->green_pressed;
  	last_knobs->blue_pressed = now->blue_pressed;
  	last_knobs->blue_val = now->blue_val;
	last_knobs->green_val = now->green_val;
	last_knobs->red_val = now->red_val;
}

/*
*	Initialize inputs, set terminal to RAW state, allocate space for knob types and create control status type.
*/
void init_input() {
	// Save terminal setting and set mode to raw
	tcgetattr(STDIN_FILENO, &termios_backup);
    struct termios raw;
    tcgetattr(STDIN_FILENO, &raw);
    raw.c_lflag &= ~(ECHO | ICANON);

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
    flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
    printf("Raw mode enabled\n");

    // Alloc space for controls and sets all flags to 0;
    control_status = (control_status_t*)malloc(sizeof(control_status_t));
    reset_controls();
    // Alloc space for knobs and set all states to current setting
    last_knobs = (knobs_t*)malloc(sizeof(knobs_t));
    copy_knobs();
}

/*
*	Return terminal to previous mode and frees allocated types
*/
void reset_input() {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &termios_backup);
    fcntl(STDIN_FILENO, F_SETFL, flags);
    printf("Termios reset\n");
    free(control_status);
    free(last_knobs);
}


/*
*	Set all control flags to FALSE
*/
void reset_controls() {
	control_status->select = FALSE;
	control_status->p1right = FALSE;
	control_status->p1left = FALSE;
	control_status->p2right = FALSE;
	control_status->p2left = FALSE;
}

/*
*	Set control flags by input character
*/
void bind_controls(char input) {
	switch(input) {
			case SELECT_KEY:
				control_status->select = TRUE;
				break;
			case PLAYER_1_RIGHT:
				control_status->p1right = TRUE;
				break;
			case PLAYER_2_RIGHT:
				control_status->p2right = TRUE;
				break;
			case PLAYER_1_LEFT:
				control_status->p1left = TRUE;
				break;
			case PLAYER_2_LEFT:
				control_status->p2left = TRUE;
				break;
		}
}

/*
*	Sets control flags by  knobs or terminal input
*/
void set_controls_by_input() {
	char input;
	// Reset controls
	reset_controls();
	set_knobs();
	// Set controls by knobs if knobs has changed, otherwise, check for keyboard input
	if(knobs_changed()) {
		bind_controls_from_knobs();
	} else {
		while(read(STDIN_FILENO, &input, 1) == 1) {
			bind_controls(input);
		}
	}
}

/*
*	Returns static instance of controls
*/
control_status_t* get_controls() {
	reset_controls();
	set_controls_by_input();
	return control_status;
}
