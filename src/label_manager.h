/*******************************************************************
  label_manager.h	- Manager taking care of text rendering


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include <string.h>

#include "graphical_manager.h"
#include "font_types.h"

void add_char(char c, unsigned int x_offset, unsigned int y_offset, uint16_t color, int size, int underlined);

void add_label(char* label, uint16_t color, int size, int underlined, int xpos, int ypos);