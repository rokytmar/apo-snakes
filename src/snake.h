/*******************************************************************
  snake.h 	- Declarations and utilities of snake entity


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef SNAKE_H
#define SNAKE_H

#include <stdlib.h>
#include <stdint.h>

#include "colors.h"

#define HEALTY GREEN_32
#define FEEDING BLUE_32
#define DEAD RED_32

#define MAX_LENGTH 32

typedef struct body_node_t {
	int x;
	int y;
	struct body_node_t* next;
} body_node;

typedef struct {
	int heading;	// Heading by id as defined in positioning.h
	int length;		// Length of snake
	body_node* head;	// First tile of the snake's linked list body
	body_node* tail;	// Last tile of snake's linked list body
	uint16_t color;	// Color of snake
	uint32_t state;		// State of snake as defined higher by color
} snake_t;

void turn_left(snake_t* snake);

void turn_right(snake_t* snake);

void move(snake_t* snake);

void enlarge_snake(snake_t* snake);

snake_t* generate_snake(uint16_t color, int snake_id);

#endif