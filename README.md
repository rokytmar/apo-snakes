Snakes
======
This work was created as a semestral project by [Martin Rokyta](https://gitlab.fel.cvut.cz/rokytmar) for APO course of summer semester of 2019/2020.
Source code can be found [here](https://gitlab.fel.cvut.cz/rokytmar/apo-snakes)
