/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  menu.c - menu and welcome screen


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "menu.h"

/*
*	Blink SNAKES title for welcome with led blinking and belt animation
*/
void show_welcome() {
	// Sett default values
	uint32_t belt_right;
	uint32_t belt_left;
	uint16_t col1 = RED;
	uint16_t col2 = BLUE;	
	fill_frame_with_color(BLACK);

	for(int i = 0; i < 12; i++) {
		belt_right = 0x3;
		belt_left = 0xC0000000;
		// Switch led and label colors
		col1 = col1 == RED ? BLUE : RED;
		col2 = col2 == RED ? BLUE : RED;
		light_left_led(col1 == RED ? RED_32 : BLUE_32);
		light_right_led(col2 == RED ? RED_32 : BLUE_32);

		add_label(TITLE, col2, 7, 1, 0, 0);
		refresh_display();

		// Belt animation
		while(belt_left) {
			belt_right <<= 1;
			belt_left >>= 1;
			light_led_belt(belt_right | belt_left);
			struct timespec waiter = {.tv_sec = 0, .tv_nsec = 3500000};
	    	clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);
		}
	}
	struct timespec waiter = {.tv_sec = 1, .tv_nsec = 0};
	clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);
	reset_display();
	reset_frame();
}

/*
*	Show menu and handle menu interactions
*/
int show_menu(game_t* game) {
	show_welcome();

	int in_menu = TRUE;
	while(in_menu) {
		//Default selector flag and selection pointer
		int button_selector = START_BUTTON;
		int option_selected = FALSE;
		while(!option_selected) {
			// Print labels to underline selected posobility
			reset_frame();
			add_label("Start game", GREEN, 5, button_selector == START_BUTTON ? TRUE : FALSE, 0, -DISPLAY_HEIGHT / 2);
			add_label("Settings", BLUE, 4, button_selector == SETTINGS_BUTTON ? TRUE : FALSE, 0, 0);
			add_label("Exit", RED, 4, button_selector == EXIT_BUTTON ? TRUE : FALSE, 0, DISPLAY_HEIGHT / 2);
			refresh_display();

			control_status_t* controls = get_controls();

			// Handle controls to change selection or select current one
			if(controls->select) {
				option_selected = TRUE;
			}
			if(controls->p1right || controls->p2right) {
				button_selector++;
				button_selector %= 3;
			}
			if(controls->p1left || controls->p2left) {
				button_selector--;
				button_selector = button_selector < 0 ? 2 : button_selector;
				button_selector %= 3;
			}
		}

		// On selection, handle the request
		switch(button_selector) {
			case START_BUTTON:
				//Exit menu and start game
				in_menu = FALSE;
				return TRUE;
				break;
			case SETTINGS_BUTTON:
				show_settings(game);
				break;
			case EXIT_BUTTON:
				//Exit menu and end game
				in_menu = FALSE;
				return FALSE;
				break;
		}
	}
	return FALSE;
}
