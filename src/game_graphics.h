/*******************************************************************
  game_graphics.h 	- Module transforming game objects to buffers and pssing them to render


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef GAME_GRAPHIC_H
#define GAME_GRAPHIC_H

#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "booleans.h"
#include "colors.h"
#include "snake.h"
#include "map.h"

#include "label_manager.h"
#include "graphical_manager.h"

#define STATUS_BAR_WIDTH 80

void draw_game(snake_t* snake1, snake_t* snake2, food_t* food);

void show_winner(int winner_id, uint16_t color);

#endif