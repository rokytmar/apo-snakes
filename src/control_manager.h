/*******************************************************************
  control_manager.h 	- Declarations and utilities of controls and control handling


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef CONTROL_MANAGER_H
#define CONTROL_MANAGER_H

#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#include "peripheral_manager.h"
#include "booleans.h"
#include "map.h"
#include "snake.h"

#define SELECT_KEY '\n'
#define PLAYER_1_RIGHT 's'
#define PLAYER_1_LEFT 'a'

#define PLAYER_2_RIGHT 'l'
#define PLAYER_2_LEFT 'k'

typedef struct {
	int select;
	int p1right;
	int p1left;
	int p2right;
	int p2left;
} control_status_t;

void init_input();

void reset_input();

void reset_controls();

control_status_t* get_controls();

#endif