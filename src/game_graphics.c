/*******************************************************************
  game_graphics.c   - Module transforming game objects to buffers and pssing them to render


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "game_graphics.h"

void draw_snake(snake_t* snake) {
    body_node* node = snake->head;
    do {
        put_tile_to_frame(snake->color, node->x * TILE_SIZE, node->y * TILE_SIZE, TILE_SIZE);
        node = node->next;
    } while (node != NULL);
}

void draw_score_box(snake_t* snake1, snake_t* snake2) {
    fill_rectangle_with_color(DISPLAY_WIDTH - STATUS_BAR_WIDTH, 0, 2, DISPLAY_HEIGHT, WHITE);
    char s1score[3];
    char s2score[3];

    sprintf(s1score, "%d", snake1->length);
    sprintf(s2score, "%d", snake2->length);

    add_label(s1score, snake1->color, 3, FALSE, DISPLAY_WIDTH - STATUS_BAR_WIDTH, -DISPLAY_WIDTH / 3);
    add_label(s2score, snake2->color, 3, FALSE, DISPLAY_WIDTH - STATUS_BAR_WIDTH, DISPLAY_WIDTH / 3);
}

void draw_game(snake_t* snake1, snake_t* snake2, food_t* food) {
    reset_frame();
    draw_score_box(snake1, snake2);
    draw_snake(snake1);
    draw_snake(snake2);
    put_tile_to_frame(FOOD_COLOR, food->x * TILE_SIZE, food->y * TILE_SIZE, TILE_SIZE);
    refresh_display();
}


void show_winner(int winner_id, uint16_t color) {
    char* winner_shoutout = (char*)malloc(14);
    sprintf(winner_shoutout, "Player %d won!", winner_id);
    reset_frame();
    printf("Game ended, player %d won!\n", winner_id);
    printf("%s\n", winner_shoutout);
    add_label(winner_shoutout, color, 5, TRUE, 0, 0);
    refresh_display();
    struct timespec waiter = {.tv_sec = 2, .tv_nsec = 0};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &waiter, NULL);
    free(winner_shoutout);
}
