/*******************************************************************
  ai_helper.c	- Declarations and utilities of artificial inteligence


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "ai_helper.h"

/*
*	Get optimal direction to get to food
*/
int get_desired_direction(snake_t* snake, food_t* food) {
	int dx = food->x - snake->head->x;
	int dy = food->y - snake->head->y;
	if(abs(dx) > abs(dy)) {
		if(dx > 0) return RIGHT;
		return LEFT;
	}
	if(dy > 0) return DOWN;
	return UP;
}

/*
*	Check if desired direction wont result in crash
*/
int direction_possible(snake_t* snake, snake_t* opponent, int desired_direction) {
	int x = snake->head->x;
	int y = snake->head->y;

	switch(desired_direction) {
		case UP:
			snake->head->y--;
			break;
		case DOWN:
			snake->head->y++;
			break;
		case RIGHT:
			snake->head->x++;
			break;
		case LEFT:
			snake->head->x--;
			break;
	}
	int will_crash = detect_crash(snake, opponent); 
	snake->head->x = x;
	snake->head->y = y;
	return !will_crash;

}

/*
*	Get new direction for snake
*/
void decide_snake_action(snake_t* snake, snake_t* opponent, food_t* food) {
	int desired_direction = get_desired_direction(snake, food);
	int first_des = desired_direction;
	while(!direction_possible(snake, opponent, desired_direction)) {
		desired_direction++;
		desired_direction %= 4;
		//If no direction possible, crash
		if(first_des == desired_direction) {
			break;
		}
	}
	snake->heading = desired_direction;
}
