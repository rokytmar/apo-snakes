/*******************************************************************
  label_manager.c	- Manager taking care of text rendering


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "label_manager.h"

static font_descriptor_t font;

/*
*	Translate characters to position in font array
*/
void chars_to_font_indexes(char* label, char* labelIn, int label_length) {
	for(int i = 0; i < label_length; i++) {
		label[i] = *(labelIn + i) - font.firstchar;
	}
}

/*
*	Add character to frame
*/
void add_char(char c, unsigned int x_offset, unsigned int y_offset, uint16_t color, int size, int underlined) {
	uint16_t* frame = get_frame();
	unsigned char char_width = font.width[(int)c];

	int offset = c * font.height;
	uint16_t line_bits;
	//Apply masks t ofont bits to determine pixels to put color to in frame
	for(int y = 3; y < font.height * size; y++) {
		line_bits = font.bits[offset + (y / size)];
		line_bits = line_bits >> (16 - char_width);

		for(int x = 0; x < char_width * size; x++) {
			if((line_bits >> (char_width - (x / size))) & 1) {
				frame[(y_offset + y) * DISPLAY_WIDTH + x_offset + x] = color;
			}
		}
	}
	// Add line below th eworld if underlined
	if(underlined) {
		for(int y = 0; y < size; y++) {
			for(int x = 0; x < char_width * size; x++) {
				frame[(y_offset + y + font.height * size) * DISPLAY_WIDTH + x_offset + x] = color;
			}
		}
	}
}

/*
* 	Add label to frame
*/
void add_label(char* labelIn, uint16_t color, int size, int underlined, int xpos, int ypos) {
	font = font_winFreeSystem14x16;
	int label_length = strlen(labelIn);
	char label[label_length];
	chars_to_font_indexes(label, labelIn, label_length);

	unsigned int label_height = font.height * size;
	unsigned int label_width = 0;
	// Determine label width to center it
	for(int i = 0; i < label_length; i++) {
		label_width += font.width[(int)label[i]];
	}
	label_width *= size;

	//Add label center to positions specified
	unsigned int x_offset = (DISPLAY_WIDTH + xpos - label_width) / 2;
	unsigned int y_offset = (DISPLAY_HEIGHT + ypos - label_height) / 2;

	for(int i = 0; i < label_length; i++) {
		add_char(label[i], x_offset, y_offset, color, size, underlined);
		x_offset += font.width[(int)label[i]] * size;
	}
}