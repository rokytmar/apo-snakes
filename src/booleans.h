/*******************************************************************
  booleans.h 	- Custom declaration of boolean variables


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef BOOLEANS_H
#define BOOLEANS_H

#define TRUE 1
#define FALSE 0

#endif