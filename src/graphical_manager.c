/*******************************************************************
  graphical_manager.c	- Simple paralel lcd driver


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#include "graphical_manager.h"

// Static memory base for lcd display
static unsigned char* lcd_mem_base;
// Static array representing frame
static uint16_t* frame;

/*
*   Malloc frame
*/ 
void generate_frame() {
	frame = (uint16_t*)malloc(DISPLAY_WIDTH * DISPLAY_HEIGHT * sizeof(uint16_t));
}

/*
*   Returns static instance of frame
*/
uint16_t* get_frame() {
  if(frame == NULL) {
    generate_frame();
  }
  return frame;
}

/*
*   Init graphics, set memory base, initialize display and generate frame
*/
void init_graphics() {
	lcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (lcd_mem_base == NULL) {
        fprintf(stderr, "There was an error mapping the lcd display\n");
        exit(1);
    }
    parlcd_hx8357_init(lcd_mem_base);
    generate_frame();
}

/*
*   Set whole frame to be black
*/
void reset_frame() {
  fill_frame_with_color(BLACK);
}

/*
*   Sets display to be black without changing frame
*/
void reset_display() {
	parlcd_write_cmd(lcd_mem_base, DISPLAY_WRITE);
    for (int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        parlcd_write_data(lcd_mem_base, BLACK);
    }
}

/*
*   Add game map tile to frame by position and tile size - as a rectangle
*/
void put_tile_to_frame(uint16_t color, int x_offset, int y_offset, int tile_size) {
  if(frame == NULL) {
    generate_frame();
  }
  for(int y = y_offset; y < y_offset + tile_size; y++) {
    for(int x = x_offset; x < x_offset + tile_size; x++) {
      if(x > 0 && x < DISPLAY_WIDTH && y > 0 && y < DISPLAY_HEIGHT) { 
        frame[y * DISPLAY_WIDTH + x] = color;
      }
    }
  }
}

/*
*   Fill specified rectangle in frame with color
*/
void fill_rectangle_with_color(int posx, int posy, int width, int height, uint16_t color) {
  if(frame == NULL) {
    generate_frame();
  }
  for(int y = posy; y < posy + height; y++) {
    for(int x = posx; x < posx + width; x++) {
      if(x > 0 && x < DISPLAY_WIDTH && y > 0 && y < DISPLAY_HEIGHT) { 
        frame[y * DISPLAY_WIDTH + x] = color;
      }
    }
  }
}

/*
*   Fill whole frame with color
*/
void fill_frame_with_color(uint16_t color) {
  if(frame == NULL) {
    generate_frame();
  }
	for(int i = 0; i < DISPLAY_WIDTH * DISPLAY_HEIGHT; i++) {
		frame[i] = color;
	}
}

/*
*   Render current frame to display
*/
void refresh_display() {
  if(frame == NULL) {
    generate_frame();
  }
	parlcd_write_cmd(lcd_mem_base, DISPLAY_WRITE);
	for(int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
		parlcd_write_data(lcd_mem_base, frame[i]);
	}
}

/*
*   Free frame and reset dispaly to black
*/
void terminate_graphics() {
	free(frame);
	reset_display();
}
