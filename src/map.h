/*******************************************************************
  map.h 	- Declarations and utilities of map, with positioning system and basic collision resolver


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef MAP_H
#define MAP_H

#include <stdlib.h>

#include "graphical_manager.h"

#include "booleans.h"
#include "colors.h"
#include "snake.h"

#define WALL_COLOR WHITE
#define FOOD_COLOR PINK

#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

#define STATUS_BAR_WIDTH 80
#define TILE_SIZE (DISPLAY_WIDTH / 20)
#define MAP_WIDTH ((DISPLAY_WIDTH - STATUS_BAR_WIDTH) / TILE_SIZE)
#define MAP_HEIGHT (DISPLAY_HEIGHT / TILE_SIZE - 1)

typedef struct {
	int x;
	int y;
} food_t;

void generate_new_food(food_t* food);

void detect_collision(snake_t* snake, snake_t* opponent, food_t* food);

int detect_crash(snake_t* snake, snake_t* opponent);

#endif