/*******************************************************************
  peripheral_manager.c 	- Simple peripheral parser


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/


#include "peripheral_manager.h"

// Static mapped adress of peripherals memory base
static unsigned char* per_mem_base;
static knobs_t* knobs;

/*
*   Get knobs static instance
*/
knobs_t* get_knobs() {
  if(knobs == NULL) {
    knobs = (knobs_t*)malloc(sizeof(knobs));
    set_knobs();
  }
  return knobs;
}

/*
*   Sets knobs struct values by value in mapped memory
*/
void set_knobs() {
  // Get all values to one 32 bit int
  uint32_t vals = *(volatile uint32_t*)(per_mem_base + SPILED_REG_KNOBS_8BIT_o);
  // Set value by last 8 bits and then bit shift to get next value
  knobs->blue_val = vals & 0xffu;
  vals >>= 8;
  knobs->green_val = vals & 0xffu;
  vals >>= 8;
  knobs->red_val = vals & 0xffu;
  vals >>= 8;

  knobs->blue_pressed = vals & 0x1u;
  vals >>= 1;
  knobs->green_pressed = vals & 0x1u;
  vals >>= 1;
  knobs->red_pressed = vals & 0x1u;
  vals >>= 1;
}

/*
*   Sets memory base for peripherals, initializes dispaly and knobs
*/
void init_peripherals() {
	  per_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (per_mem_base == NULL) {
        fprintf(stderr, "There was an error mapping peripherals\n");
        exit(1);
    }
    parlcd_hx8357_init(per_mem_base);
    knobs = (knobs_t*)malloc(sizeof(knobs_t));
    set_knobs();
}

/*
*   Lights led on offset from memory base to color
*/
void light_led(uint32_t color, uint16_t offset) {
	*(volatile uint32_t*)(per_mem_base + offset) = color;
}

/*
*   Lights left led color
*/
void light_left_led(uint32_t color) {
	light_led(color, SPILED_REG_LED_RGB1_o);
}

/*
*   Lights left led color
*/
void light_right_led(uint32_t color) {
	light_led(color, SPILED_REG_LED_RGB2_o);
}

/*
*   Lights led belt by 32 bit value
*/
void light_led_belt(uint32_t value) {
	*(volatile uint32_t*)(per_mem_base + SPILED_REG_LED_LINE_o) = value;
}

/*
*   Converts integer to number of bits from right
*/
uint32_t integer_to_bits(uint32_t num) {
   uint32_t bits = 0;
   for(int i = 0; i < num; i++) {
      bits >>= 1;
      bits |= 0x80000000;
   }
   return bits;
}

/*
*   Light belt by composing two 32 bit ints
*/
void light_belt_bits(int num1, int num2) {
    uint32_t bits1;
    uint32_t bits2;

    bits1 = integer_to_bits(num1);
    bits2 = integer_to_bits(num2) >> 16;
    light_led_belt(bits1 | bits2);
}

/*
*   Shuts display and leds and frees knobs
*/
void reset_peripherals() {
	light_led(BLACK, SPILED_REG_LED_RGB1_o);
	light_led(BLACK, SPILED_REG_LED_RGB2_o);
	light_led_belt(0);
  free(knobs);
}
