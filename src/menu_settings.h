/*******************************************************************
  menu_settings.h 	- Menu page for setting selection


  (C) Copyright 2020 by Martin Rokyta
      e-mail:   rokytmar@fel.cvut.cz
      homepage: https://gitlab.fel.cvut.cz/rokytmar
      license:  any combination GPL, LGPL, MPL or BSD licenses
 *******************************************************************/

#ifndef MENU_SETTING_H
#define MENU_SETTING_H

#include <stdlib.h>

#include "graphical_manager.h"
#include "peripheral_manager.h"
#include "control_manager.h"
#include "label_manager.h"
#include "colors.h"
#include "booleans.h"
#include "game.h"

#define SPEED_SELECTOR 0
#define P1_COLOR_SELECTOR 1
#define P2_COLOR_SELECTOR 2
#define BACK_BUTTON 3

#define SLOW_LABEL "<SLOW>"
#define MEDIUM_LABEL "<MEDIUM>"
#define FAST_LABEL "<FAST>"

#define COLOR_TILE_SIZE 20

#ifndef GAME_TYPE
#define GAME_TYPE
typedef struct {
	int game_running;		//Game running flag
	int regime;				//Game regime selector
	int game_speed;			//Game speed selector
	uint16_t snake1_color;	//Color of first snake
	uint16_t snake2_color;	//Color of second snake
} game_t;
#endif

void show_settings(game_t* game);

#endif